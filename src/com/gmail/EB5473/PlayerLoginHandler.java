package com.gmail.EB5473.RealCityBase;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class PlayerLoginHandler implements Listener {

	RealCity plugin;
	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event, RealCity plugin)
	{
		this.plugin = plugin;
		event.getPlayer().sendMessage(plugin.getConfig().getString("MOTD"));
	}
}
