package com.gmail.EB5473.RealCityBase;

import java.util.Iterator;
import java.util.Map;

import javax.swing.text.StyledEditorKit.ForegroundAction;

import org.bukkit.block.Block;
import org.bukkit.plugin.java.JavaPlugin;

import com.avaje.ebean.Update;

public class GeneratorStateHandler {
	RealCity plugin;
	public GeneratorStateHandler(RealCity plugin)
	{
		this.plugin = plugin;
	}
	public  void update() 
	{
		Iterator it = plugin.generatorState.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pairs =(Map.Entry)it.next();
			if(plugin.generators.containsKey(pairs.getKey()) && plugin.generatorState.get(pairs.getKey()) == true)
			{
				Block block = plugin.generators.get(pairs.getKey()).getBlock();
				block.getWorld().createExplosion((double)block.getX(),(double)block.getY(),(double)block.getZ(),10f,true);
						
			}
		}
	}
}
